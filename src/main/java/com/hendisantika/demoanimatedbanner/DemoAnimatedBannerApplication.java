package com.hendisantika.demoanimatedbanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoAnimatedBannerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoAnimatedBannerApplication.class, args);
    }
}
